using Microsoft.AspNetCore.Mvc;
using MainFolder.Functions;
using MainFolder.Models;

namespace MainFolder.Controllers;

[ApiController]
[Route("api/tasks")]
public class TasksController : ControllerBase
{

  [HttpGet]
  [Route("GetUserWithTask")]
  public ActionResult Get(string? name)
  {
    try
    {
      return Ok(TasksFunction.GetUsers(name));
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
  }

  [HttpPost]
  [Route("AddUserWithTask")]
  public ActionResult Post([FromBody] Users userData)
  {
    try
    {
      return Ok(TasksFunction.AddUsers(userData));
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
  }
}
