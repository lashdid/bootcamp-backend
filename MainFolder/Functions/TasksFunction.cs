using MainFolder.Models;
using MainFolder.Logics;
using System.Data;
using System.Data.SqlClient;
namespace MainFolder.Functions;

public class TasksFunction
{
  public static List<UsersWithId> GetUsers(string? name = null)
  {
    List<UsersWithId> result = new List<UsersWithId>();

    string usersQuery = "SELECT * FROM users";
    SqlParameter[]? nameParams = null;

    if (!string.IsNullOrEmpty(name))
    {
      usersQuery = "SELECT * FROM users WHERE users.name LIKE @name";
      nameParams = new SqlParameter[]
      {
          new SqlParameter("@name",SqlDbType.VarChar){Value = $"%{name}%" }
      };
    }


    DataTable usersTable = UseQuery.ExecuteQuery(usersQuery, nameParams);

    foreach (DataRow userRow in usersTable.Rows)
    {
      SqlParameter[] userIdParams = new SqlParameter[]
      {
        new SqlParameter("@userId",SqlDbType.Int){Value = (int)userRow["pk_users_id"] }
      };
      string tasksQuery = "SELECT * FROM tasks WHERE fk_users_id = @userId";
      DataTable tasksTable = UseQuery.ExecuteQuery(tasksQuery, userIdParams);
      List<TasksWithId> userTasks = new List<TasksWithId>();
      foreach (DataRow taskRow in tasksTable.Rows)
      {
        TasksWithId tempTasks = new TasksWithId
        {
          pk_tasks_id = (int)taskRow["pk_tasks_id"],
          task_detail = (string)taskRow["task_detail"],
        };
        userTasks.Add(tempTasks);
      }
      UsersWithId tempUsers = new UsersWithId
      {
        pk_users_id = (int)userRow["pk_users_id"],
        name = (string)userRow["name"],
        tasks = userTasks
      };
      result.Add(tempUsers);
    }

    return result;
  }

  public static object AddUsers(Users userData)
  {
    try
    {
      SqlParameter[] nameParams = new SqlParameter[]
      {
      new SqlParameter("@name",SqlDbType.VarChar){Value = userData.name }
      };
      string insertQuery = "INSERT INTO users (name) VALUES(@name); SELECT SCOPE_IDENTITY();";

      decimal userId = (decimal)UseQuery.ExecuteScalar(insertQuery, nameParams);

      foreach (Tasks task in userData.tasks ?? new List<Tasks>())
      {
        SqlParameter[] taskParams = new SqlParameter[]
        {
          new SqlParameter("@detail",SqlDbType.VarChar){Value = task.task_detail },
          new SqlParameter("@userId",SqlDbType.Int){Value = userId}
        };
        string insertTaskQuery = "INSERT INTO tasks (task_detail, fk_users_id) VALUES(@detail, @userId)";
        UseQuery.ExecuteNonQuery(insertTaskQuery, taskParams);
      }
      return (new {userId = userId, status = "Berhasil", message = "User telah ditambahkan" });
    }
    catch (Exception)
    {
      throw;
    }
  }
}