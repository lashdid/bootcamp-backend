namespace MainFolder.Models;

public class Users
{
    public string? name  { get; set; }
    public List<Tasks>? tasks {get; set;}
}

public class UsersWithId : Users {
    public int pk_users_id { get; set; }
    public new List<TasksWithId>? tasks {get; set;}
}