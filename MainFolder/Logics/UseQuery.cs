using System.Data;
using System.Data.SqlClient;

namespace MainFolder.Logics;
public class UseQuery
{
  // private static string connectionString = "Server=localhost;Database=task6;User Id=sa;Password=Alhan1903;";
  private static string connectionString = "";

  public static void GetConfiguration(IConfiguration configuration)
  {
    connectionString = configuration["ConnectionStrings:Default"];
  }
  public static DataTable ExecuteQuery(string sql, SqlParameter[]? sqlParameters = null)
  {
    DataTable result = new DataTable();

    #region query proccess database
    using (SqlConnection conn = new SqlConnection(connectionString))
    {
      conn.Open();

      #region query
      using (SqlCommand cmd = new SqlCommand(sql, conn))
      {
        // kenapa tidak diberikan transaction?
        // karena menurut saya method ExecuteQuery hanya digunakan untuk SELECT query saja
        // dan tidak akan mengubah apa-apa, jadi untuk apa di beri rollback
        try
        {
          if (sqlParameters != null)
          {
            cmd.Parameters.AddRange(sqlParameters);
          }
          SqlDataAdapter adapter = new SqlDataAdapter(cmd);
          adapter.Fill(result);
        }
        catch (Exception)
        {
          throw;
        }
      }
      conn.Close();
      #endregion
    }
    #endregion

    return result;
  }

  #region ExecuteScalar
  /// <summary>
  /// ExecuteScalar untuk menjalankan query yang hanya return tepat 1 data
  /// </summary>
  public static object ExecuteScalar(string query, SqlParameter[]? sqlParameters = null)
  {
    object? result;

    // begin connection
    using (SqlConnection con = new SqlConnection(connectionString))
    {
      con.Open();

      #region query process to database
      using (SqlCommand cmd = new SqlCommand(query, con))
      {
        cmd.Connection = con;
        cmd.Transaction = con.BeginTransaction();
        try
        {
          if (sqlParameters != null)
          {
            cmd.Parameters.AddRange(sqlParameters);
          }
          result = cmd.ExecuteScalar();
          cmd.Parameters.Clear();
          cmd.Transaction.Commit();
          con.Close();
        }
        catch (Exception)
        {
          cmd.Transaction.Rollback();
          con.Close();
          throw;
        }
      }
      #endregion
    }

    return result;
  }
  #endregion

  #region ExecuteNonQuery
  /// <summary>
  /// ExecuteNonQuery untuk menjalankan query yang tidak return apa-apa
  /// </summary>
  public static int ExecuteNonQuery(string query, SqlParameter[] sqlParameters)
  {
    int result = 0;

    // begin connection
    using (SqlConnection con = new SqlConnection(connectionString))
    {
      con.Open();

      #region query process to database
      using (SqlCommand cmd = new SqlCommand(query, con))
      {
        cmd.Connection = con;
        cmd.Transaction = con.BeginTransaction();
        try
        {
          cmd.Parameters.AddRange(sqlParameters);
          result = cmd.ExecuteNonQuery();
          cmd.Parameters.Clear();
          cmd.Transaction.Commit();
          con.Close();
        }
        catch (Exception)
        {
          cmd.Transaction.Rollback();
          con.Close();
          throw;
        }
      }
      #endregion
    }

    return result;
  }
  #endregion
}
